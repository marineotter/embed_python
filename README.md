## 概要

Pythonを環境への影響を最小限に実行するためのもの。

Python embeddableを使い、バージョンは3.8.3で仮組み。

## 使い方

### 準備

初回は`prepare.bat`を実行する。インターネット環境が必要。

プロンプトが閉じ、生成されたbinディレクトリの中に色々入っていたら多分成功。

### 準備の確認

環境にpythonが入っていない環境で以下を実行。

`execshell.bat`を実行し、 `python --version`コマンドが正しく実行できれば◎

### インタフェース

#### execshell

`execshell.bat`はPythonが有効になったcmdシェルを提供する。  
デバッグ用途が主だが、シェル側でなにかしたい場合に使える。

batファイルに与えた引数は全てcmd.exeに引き渡される。

`execshell.bat /c pip install -r requirements.txt` とかがよくあるケースか。動くかは知らない。

#### execpy

`execpy.bat`はpythonコマンドの薄いラッパー。

こちらも引数全てをpythonコマンドに渡して実行する。

## ライブラリの導入について

`execshell.bat`で起動したコンソールの上で、`pip`が使えます。

embeddable pythonをつかっているので、コンパイルが必要なライブラリなどは導入に苦労するかもしれません。

また、自製共有モジュールについては、`PYTHONPATH`をsite-packageフォルダに通しているため、そこに入れてください。  
注記：本実装によるインタフェース使用時にはPYTHONPATHを引き継がないため、親シェルでPYTHONPATHを通してそこに置く手法は使いにくいです。

おまけですが、`prepare.bat`実行時に同じディレクトリにrequirements.txtを置いておくと、頑張ってインストールしようとします。

## 環境の作り直しについて

`bin`フォルダを削除して、`prepare.bat`を実行し直して下さい。

pythonのDLもやり直す場合、`python.zip`も削除して下さい。

## IDEとの結合について

実行時環境は`setenv.bat`が全てです。このファイルを参考に、各種環境変数を設定します。
