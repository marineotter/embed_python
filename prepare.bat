cd %~dp0
if not exist .\bin\python.exe (
    if not exist .\python.zip (
        powershell -c invoke-webrequest -uri https://www.python.org/ftp/python/3.8.3/python-3.8.3-embed-win32.zip -outfile .\python.zip
    )
    powershell -c Expand-Archive -Force -Path .\python.zip -DestinationPath .\bin
    REM del .\python.zip
)

if not exist .\bin\python38.pth (
    powershell -c "$result = $(Get-Content .\bin\python38._pth) -replace '#import site', 'import site'; $result > .\bin\python38.pth"
    del .\bin\python38._pth
)

if not exist .\site-packages (
    mkdir site-packages
)

call %~dp0setenv.bat

if not exist .\bin\get-pip.py (
    powershell -c invoke-webrequest -uri https://bootstrap.pypa.io/get-pip.py -outfile .\bin\get-pip.py
    python bin\get-pip.py
)

if exist .\requirements.txt (
    pip install -r requirements.txt
)

python --version
